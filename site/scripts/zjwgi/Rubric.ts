/**
 * \author Zachary Wartell
 * \copyright Copyright 2015. Zachary Wartell.
 * \license Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
 * http://creativecommons.org/licenses/by-nc-sa/4.0/
 */

/**
 **
 **  EXPORTED FUNCTIONS, CLASSES, ETC.
 **
 */

 
 function AUTO_DOC()
 {
    //stub
    return new String;
 }

 /**
  * [wip] \todo AUTO_DOC a mechanism for generating user documentation and writing the text next the source code that directly implements the user visible concepts.
  */
 AUTO_DOC["Instruction Category"]=
 `
    <ol> These are the CSS class for ol, li or section HTML::Elements that map to Instruction object (of enum Category that is either individual or composite)
        <li> Instruction_Question - the li element asks a question the student must answer.   The method of submitting the must be descrbed within li element content.
        </li>
        <li> Instruction_Read - the li element listed required reading only.
        </li>
        <li> Instruction_Todo - the li element is a specific todo action item instruction
        </li>
        <li> Instruction_Overview - the li element is an textual overview of the nested set of li Instruction elememnts
        </li>        
        <li> Instruction_General - the li element is an general instruction 
        </li>        
        <li> Instruction_Reminder - the li element has no points assigned to it in the Rubric
        </li>        
        <li> Instruction_Section - the section element will have its own entry in the Rubric table
        </li>        
        <li> Instruction_Composite - the li element will is tagged as a composition instruction, and li element containing at nest ol element and containing nested instructions
        </li>  
        <li> Instruction_Git_Commit - the li element will is tagged as a git commit operation
        </li>        
        <li> Instruction_NonRubric - the li element will be ignored by the Rubric generation algorithm
        </li>        
    </ol>
 `;
 
/*
 * @brief Category is a kind of Instruction.  There is a 1-to-1 mapping between enum Category values and CSS classes with names matching the regex "Instruction.*"
 * @see AUTO_DOC["Instruction Category"] 
 */
enum Category {
    AUTO = "AUTO",
    /* Instruction instance is associated with a a HTML <section> */
    SECTION = "SECTION",
    /* Instruction instance is associated with a  HTML <li> that has a child instructional <ol> tree */
    COMPOSITE = "COMPOSITE",
    /* Instruction instance is associated with a question HTML <li> */
    QUESTION = "QUESTION",
    /* Instruction instance is associated with a reading HTML <li> */
    READ = "READ",
    /* Instruction instance is associated with a todo HTML <li> */
    TODO = "TODO",
    /* Instruction instance is associated with a reminder HTML <li>. A Reminder Instructino has point fraction = 0 */
    REMINDER = "REMINDER",
    /* [considered for deprecation] Instruction instance is associated with a question HTML <li> */
    GENERAL = "GENERAL",
    OVERVIEW = "OVERVIEW",
    NON_RUBRIC = "NON_RUBRIC",
    GIT_COMMIT = "GIT_COMMIT"
}

/** 
 * @param element 
 * @param returnNull - this is a vestigate of earlier code, eventually this parameter should be removed and all code refactored
 * @returns 
 */
function getCategoryFromClass(element : HTMLElement, returnNull : boolean)
{
    if (element.className.includes("Instruction_Question"))
        return Category.QUESTION;
    if (element.className.includes("Instruction_Read"))
        return Category.READ;
    if (element.className.includes("Instruction_Todo"))
        return Category.TODO;
    if (element.className.includes("Instruction_Overview"))
        return Category.OVERVIEW;
    if (element.className.includes("Instruction_General"))
        return Category.GENERAL;
    if (element.className.includes("Instruction_NonRubric"))
        return Category.NON_RUBRIC;
    if (element.className.includes("Instruction_Reminder"))
        return Category.REMINDER;
    if (element.className.includes("Instruction_Section"))
        return Category.SECTION;
    if (element.className.includes("Instruction_Composite"))
        return Category.COMPOSITE;
    if (element.className.includes("Instruction_Git_Commit"))
        return Category.GIT_COMMIT;        

    if (returnNull)
        return null;
    else
        return Category.AUTO;
}



/**
 * \brief [status: thought stage] some tutorials assignments have different options for students with different levels of past experiences
 */
export class OptionSet 
{
    name: string;
    options: Array<Section>;    
    constructor(n : string)   
    {
        this.name = n;
        this.options = new Array<Section>;
    }

    static optionSetByName : Map<string,OptionSet> = new Map<string,OptionSet>();
}


/**
 * @brief A Section corresponds to a <section> in the HTML document and contains Instruction objects (which correspond to HTMLElements with class="Instruction_*") and child Section's (which correspond to the child <section>'s)
 */
export class Section
{    
    name: string;  // name of section
    sectionNumber : string;   // full section number 
    numeral : string; // number within current level, could be Arabic or Roman number, upper or lower case
    number: number;  // number within current level
    level : number;  // depth of nesting in <section> tree
    optionSet : OptionSet | null =null;  // can only be !== null when this.level === 1
    optionIndex : number = 0;  // index of this Section within it's OptionSet (if optionSet !== null)
    id: string;      // HTML id attribute of <section>

    cumulativeFraction : number = 0;

    parent : Section;          // parent Section
    children : Array<Section>; // child Section    

    instruction : Instruction | null = null;  // associated Instruction

    constructor(name : string, parent : Section, number : number)
    {
        this.name = name;

        this.parent = parent;
        this.children = new Array<Section>();
        this.level = 1;
        if (parent !== null)
        {
            parent.children.push (this);
            this.sectionNumber = parent.sectionNumber + "." + number.toFixed(0);
            for (let p = parent; p !== null; p = p.parent)            
                this.level++;
        }
        else        
            this.sectionNumber =  number.toFixed(0);            

        // \todo [refactor] calculate id in this constructor instead
        this.id = "Section_"+this.sectionNumber;        

        Section.sections.push(this);
    }
    
    /**
     * @brief [wip?] buildList construct TableOfContents HTML elements 
     * @param section 
     * @param ul 
     */
    static buildList(section : Section , ul : HTMLUListElement)
    {
        const li : HTMLLIElement = document.createElement("li");            
        li.innerHTML = `${section.sectionNumber} <a href="#${section.id}">${section.name}</a>`;
        ul.appendChild(li);    
        if (section.children.length !== 0)
        {
            const ul1 : HTMLUListElement = document.createElement("ul");                    
            li.appendChild(ul1);              
                                              
            ul1.classList.add("side-nav-bar");
            for(let s of section.children)
                Section.buildList(s,ul1);
        }
    }

    static displayTableOfContents()
    {
        const snb : HTMLDivElement = <HTMLDivElement>document.querySelector("div#side-nav-bar > div");
        if (snb !== null)
        {
            const ul : HTMLUListElement = document.createElement("ul");        
            snb.appendChild(ul);
            ul.classList.add("side-nav-bar");
            for (let s of Section.sections)        
            {
                if (s.level===1)
                    Section.buildList(s,ul);
            }
        }
    }
    /**
     * array of all Section objects
     */
    static sections : Array<Section>;
} 

export class Sequence
{
    sections : Array<Section> = new Array<Section>;
    totalPoints : number = 0;
    constructor()
    {

    }

}
Section.sections = new Array<Section>();

// 4/12/2023: unused , delete if still unsed after a few months, brainstorming idea
enum PointCalculation 
    {
        MANUAL_OVERRIDE,     /* Instruction.awarded value manually entered */
        COMPOSITE,           /* Instruction.awarded calculated as sum of descendents */
        FULL_OR_ZERO         /* Instruction.awarded value is full credit or zero */
    }    

/**
 * \brief Instruction is a instruction (or task) in assignment.  Instructions can be hierarchical composites of other sub Instructions and of different
 * enum Category. 
 * \author Zachary Wartell
 */    
export class Instruction {

    section: string;        // \todo [refactor] replace with class Section object

    number: string;         
    id: string;             // HTML id attribute that hyperlinks to <li> containing this Instruction

    pointFraction: number;  // percentage of parent instruction's total points that this instruction item is worth
    points: number;         // actual points this Instructions's task is worth out of assignment total points.
    awarded: number;          // how many points student received for this task
    comment: string;
    short: string;
    category: Category;
    pointCalculation : PointCalculation;

    subSteps: Array<Instruction>;   // sub Instructions (children)
    parent: Instruction;            // parent Instruction

    constructor(s: string = "", n: string = "", sh: string = "", c: Category = Category.GENERAL, pointFraction: number = 0, parent: Instruction = null) {
        this.section = s;
        this.number = n;
        this.short = sh;
        this.category = c;
        this.id = "Section_" + (s + "_Item_" + n).replace(/\./g, '_');
        this.pointFraction = pointFraction;
        this.points = 0;
        this.awarded = 0;
        this.comment = "";
        this.pointCalculation = PointCalculation.FULL_OR_ZERO;
        this.parent = parent;
        this.subSteps = new Array<Instruction>();
        if (this.parent !== null)
        {
            parent.pointCalculation = PointCalculation.COMPOSITE;
            parent.category = Category.COMPOSITE;
            console.assert(this.parent !== undefined);
            console.assert(this.parent.subSteps !== undefined);
            this.parent.subSteps.push(this);
        }
    }
    assign(jsonObject: Object) {
        for (let p in Object)
            if (p in this)
                this[p] = jsonObject[p]
    }


   
    /**
     * \brief update to this Instruction.awarded points based on GUI checkboxchange and handle upward and downward
     *  propagation of checkbox changes based on Instruction hierarchy
     * @param input 
     */    
    gui_checkbox(input : HTMLInputElement) : void
    {
        this.gui_checkbox_recursive(input);
        
        instructions.recalc_points();
        instructions.gui_update_awarded();
        (<HTMLSpanElement>document.getElementById("AwardedPoints")).innerText = instructions.awardedPoints.toFixed(2);

        /* this causes the Rubric .html that is saved to a fill, to have it's checkbox HTML Attribute 'default'
           set to the DOM checkbox's run-time state
         */
        input.defaultChecked = input.checked;
    }

    /**
     * \brief update to this Instruction.awarded points based on GUI checkboxchange and handle upward and downward
     *  propagation of checkbox changes based on Instruction hierarchy
     * @param input 
     */
    gui_checkbox_recursive(input : HTMLInputElement, processChildren  : Boolean = true)
    {
        const oldAwarded = this.awarded;
        if (input.checked)
            {// checkbox chedked, set awarded points, this.awarded, to this.points                
                const i = instructions.instructions.findIndex((element) => element == this);
                const td : HTMLElement = document.querySelector("table.Rubric > tbody > tr[data-ri='"+i.toString()+"'] > td:nth-child(6) > span:nth-child(2) > span:nth-child(2)");

                this.awarded = this.points;
                td.innerHTML = this.awarded.toFixed(2);

                input.classList.remove("Grey");            
                for (let c of this.subSteps)
                    {
                    const ci = instructions.instructions.findIndex((element) => element == c);
                    const tr : HTMLElement = document.querySelector("table.Rubric > tbody > tr[data-ri='"+ci.toString()+"'");
                    console.assert(tr !== null);
                    const childCB : HTMLInputElement = tr.querySelector(":scope input[type='checkbox']");
                    console.assert(childCB !== null);                                            
                    childCB.checked = true;
                    c.gui_checkbox_recursive(childCB);
                    }

                /* this causes the Rubric .html that is saved to a fill, to have it's checkbox HTML Attribute 'default'
                    set to the DOM checkbox's run-time state
                */
                input.defaultChecked = input.checked;
            }
        else
            {// checkbox unchecked, reset Instruction.awarded points to 0 (and adjust Instruction.subStep hierarchy as needed)                
                const i = instructions.instructions.findIndex((element) => element == this);
                const td : HTMLElement = document.querySelector("table.Rubric > tbody > tr[data-ri='"+i.toString()+"'] > td:nth-child(6) > span:nth-child(2) > span:nth-child(2)");

                this.awarded = 0;
                td.innerHTML = this.awarded.toFixed(2);
                
                input.classList.remove("Grey");
                if(oldAwarded !== this.awarded)
                {// box was checked and now unchecked, so reset all parent Instructions                    
                    for (let p = this.parent; p !== null; p = p.parent)
                    {                               
                        /* get GUI <tr> element contains Instruction at level above this Instruction */
                        const pi = instructions.instructions.findIndex((element) => element == p);
                        console.assert(pi !== -1);                 
                        const parentCB : HTMLInputElement = document.querySelector("table.Rubric > tbody > tr[data-ri='"+pi.toString()+"'] input[type='checkbox']");
                        console.assert(parentCB !== null);                                                                
                        parentCB.checked = false;                                
                        parentCB.classList.add("Grey");                            
                        p.gui_checkbox_recursive(parentCB,false);                  
                        parentCB.classList.add("Grey");                                      
                    }            
                    /*
                    Goal:  uncheck child Instructions 
                    Bug:   right now this code causes all sorts of problems , disabled for now
                    */
                    if (processChildren)
                        for (let c of this.subSteps)
                        {
                            const i = instructions.instructions.findIndex((element) => element == c);
                            const tr : HTMLElement = document.querySelector("table.Rubric > tbody > tr[data-ri='"+i.toString()+"'");
                            console.assert(tr !== null);
                            const cInput : HTMLInputElement = tr.querySelector(":scope input[type='checkbox']");
                            console.assert(cInput !== null);                        
                            cInput.checked = false;                            
                            c.gui_checkbox_recursive(cInput);                        
                            cInput.classList.remove("Grey");
                        }                                                     
                }
                    
            }                    
    }

    /**
     * \brief replacer callback for JSON.stringify
     * @param key 
     * @param value 
     * @returns 
     */
    static replacer(key: any, value: any) {
        if (key === 'parent')
            return instructions.instructions.indexOf(value);
        if (key === 'subSteps') {
            const subSets: Array<number> = new Array<number>;
            for (let s of value)
                subSets.push(instructions.instructions.indexOf(s));
            return subSets;
        }
        return value;
    }
}

/**
 * @author Zachary Wartell
 * @brief BreadCrumb and BreadCrumbs are used to navigate precisely (forward and back) within the page uses the DOM .scrollIntoView() function which is much more precise
 * then simply letting the browser jump to within page hyperlinks
 * 
 * @status [IN PROGRESS] partial implementation, disabled except in "TA Mode"
 */
export class BreadCrumb
{
    target : HTMLElement ;

    constructor( target : HTMLElement )
    {
        this.target = target;
    }

    static onclick(anchor : HTMLAnchorElement )
    {   
        anchor.id = "BC_" + Date.now().toString();              
        
        //const url = new URL(window.location);        
        //window.history.pushState(null,url.toString()+"#"+anchor.id);
        anchor.setAttribute("anchor",anchor.id);
        window.history.pushState({},"",window.location.pathname+"#"+anchor.id);                        
        console.log("BreadCrumb.onclick:", anchor, "state" , window.history.state);
        const target : HTMLElement = document.getElementById(anchor.getAttribute("href").split('#')[1]);
        target.scrollIntoView(true);
        BreadCrumbs.singleton.array.push(new BreadCrumb(anchor));
        BreadCrumbs.singleton.array.push(new BreadCrumb(anchor));
        BreadCrumbs.singleton.cursor += 2;
    }    
}

/**
 * @author Zachary Wartell
 * @bug this is BreadCrumb mechanism is failing occasionally for certain internal links
 * @brief set of BreadCrumb's  
 */
export class BreadCrumbs
{
    array : Array<BreadCrumb>;
    cursor : number;

    constructor()
    {
        this.array = new Array<BreadCrumb>();
        this.cursor = 0;
    }

    static singleton : BreadCrumbs;
}

BreadCrumbs.singleton = new BreadCrumbs();

/**
 * \author Zachary Wartell
 */
export class Instructions 
{
    instructions: Array<Instruction>;
    optionSets: Array<OptionSet>;  // not used yet, just idea in planning

    totalPoints: number;
    awardedPoints : number;
    constructor() {
        this.instructions = [];
        this.optionSets = [];
        this.totalPoints = 100;
        this.awardedPoints = 0;
    }

    push(i: Instruction) 
    {
        this.instructions.push(i);
    }

    /**
     *   Update the awared points column in the Rubric <table> as well as any Grey check boxes
     */
    gui_update_awarded()
    {
        let rubric: HTMLTableSectionElement = document.querySelector("#RubricTable > tbody");
        let trs : NodeList = rubric.querySelectorAll("tr");
        let i=0;
        for (let tr of trs)
        {
            const td : HTMLElement = (<HTMLElement>tr).querySelector(":scope td:nth-child(6) > span:nth-child(2) > span:nth-child(2)");
            if (td !== null)
            {
                const instruction=this.instructions[i];
                td.innerText = instruction.awarded.toFixed(2);
                if (instruction.category === Category.COMPOSITE)
                {
                    const checkbox : HTMLInputElement = (<HTMLElement>tr).querySelector(":scope input[type='checkbox']");
                    if ( Math.abs(instruction.awarded-instruction.points) < 1e-5)
                    {                        
                        instruction.awarded = instruction.points;
                        checkbox.classList.remove("Grey");
                        checkbox.checked = true;    
                    }
                    else
                    {
                        checkbox.classList.add("Grey");                        
                        checkbox.checked = false;    
                    }
                }
                i++;
            }            
            
        }
    }

    /**
     * \brief recalculate all Instruction.points based on Instruction subStep hierarchy
     */
    recalc_points_resursive( i : Instruction ) : number
    {
        //if (i.pointCalculation === PointCalculation.COMPOSITE)
        if (i.category === Category.COMPOSITE)
        {
            i.awarded = 0;
            for (let c of i.subSteps)
                i.awarded += this.recalc_points_resursive(c);
            return i.awarded;
        }
        else    
            return i.awarded;
    }

    /**
     * @brief recalculate all Instruction.points based on Instruction subStep hierarchy
     */    
    recalc_points()
    {
        this.awardedPoints = 0;
        for (let i of this.instructions)
            if (i.parent === null)
            {
                this.recalc_points_resursive(i);            
                this.awardedPoints += i.awarded;
            }        
    }

    /**
     * @brief collectInstructions_recursive resursively extracts Instructions from nested ol.Instructions within the <section> of the document "section"     
     **/
    private collectInstructions_recursive(
        section: Section,             // the Section object we are extracting from
        sectionElement : HTMLElement, // the HTML <section> element corresponding to the above "section"
        sectionLabel: string,   // name of the section , e.g. 4.1 or 4.2.3
        parent: Instruction,    // parent Instruction (if any) that contains (as subSteps) all Instructions being extracted from the "olList"
        itemLevels : Array<number>,  // array (if any) of index numbers of nested <li> items (with respect to their own <ol>) that contain the "olList"
        olList : NodeList)  //  NodeList of all <ol> elements in the <section> "section"  from which we are recursively extracting more <li> items
    {
        let lic = 1

        for (let ol of olList) 
        {            
            let category = getCategoryFromClass(<HTMLElement>ol, false);
            const li1List = (<HTMLOListElement>ol).querySelectorAll(":scope > li");
            
            /*
            count number of items with an unassigned data-point-fraction that contribute points to the rubric
            */
            let rubricItems : number = 0;
            let unassignedRubricItems : number = 0;
            let assignedTotal : number = 0;
            for (let li_ of li1List)
            {
                const li: HTMLElement = <HTMLElement>li_;
                let tmp, cat = (tmp = getCategoryFromClass(li, true)) !== null ? tmp : category;
                if (tmp !== Category.NON_RUBRIC)
                {
                    if ('pointFraction' in li.dataset)                        
                        assignedTotal += parseFloat(li.dataset.pointFraction);
                    else
                        unassignedRubricItems++;
                    rubricItems++;
                }                    
            }                
            console.log("rubricItems:",rubricItems);
            console.log("unassigned rubricItems:",unassignedRubricItems);

            const equalFraction1: number = (100 - assignedTotal) / unassignedRubricItems;
            lic = 1;
            /*
            **  Collection level 1 <li> Instructions
            */
            for (let li_ of li1List)
            {
                const li: HTMLElement = <HTMLElement>li_;
                let tmp, cat = (tmp = getCategoryFromClass(li, true)) !== null ? tmp : category;

                if (tmp === Category.NON_RUBRIC)
                    continue;

                itemLevels.push(lic);
                this.instructions.push(new Instruction(sectionLabel, itemString(...itemLevels),
                    li.innerText.trimStart().slice(0, 10) + " ...", cat, 'pointFraction' in li.dataset ? parseFloat(li.dataset.pointFraction) : equalFraction1, parent));

                const parentLI = this.instructions[instructions.instructions.length - 1];
                if (parentLI.category == Category.AUTO)
                    parentLI.category = Category.TODO;

                li.id = this.instructions[this.instructions.length - 1].id;
                
                const liOList  = li.querySelectorAll(":scope > ol, :scope > ul");
                if (liOList !== null && liOList.length)
                    parentLI.category = Category.COMPOSITE;
                    this.collectInstructions_recursive(section,sectionElement,sectionLabel,parentLI,itemLevels,liOList);

                itemLevels.pop();
                lic++;
            }
        }

    }

    /**
     * @brief collectInstructions extracts all the instructions embedded in the HTML document <section> "section"
     */
    private collectInstructions(section: Section, sectionElement : HTMLElement, sectionLabel: string, parent: Instruction) 
    {
        let olList = sectionElement.querySelectorAll(":scope > ol.Instruction, :scope > ul.Instruction");
        const itemLevels = new Array<number>;
        this.collectInstructions_recursive(section,sectionElement,sectionLabel,parent,itemLevels,olList);
    }


    /**
     * @brief extractSectionsAndRubric recursively traverses nested <section> elements in the DOM, creation Section objects and constructing Instruction objects
     * the corresponding to <ol.Instruction> <li> HTML elements.
     **/
    extractSectionsAndRubric(
            parent : Section,               // the Section who we will recursively search for sub-<section>
            sectionElement : HTMLElement,   // <section> corresponding the Section object 'parent'
            level : number)                 // the depth of recursion into the DOM's nesting of <section> HTML elements
        {        
        /**
         *   <section> <h1>
         */
        let hList = sectionElement.querySelectorAll(":scope section > h" + level.toFixed(0));  // headingList
        if (hList !== null && hList.length !== 0)
        {
            let hc = 1; // 'headingCount'
            let ISectionParent1 : Instruction | null = null;                    
            for (let h of hList) 
            {
                const sectionElement: HTMLElement = h.parentElement;
                const sectionName : string = (<HTMLElement>h).innerText;            
                console.log(`h${level}: `, sectionName );
                console.assert(sectionElement.tagName === "SECTION");
    
                const section : Section = new Section(sectionName ,parent,hc);
                if (level === 1)
                {
                    if (sectionElement.dataset['optionset'] !== undefined)
                    {
                        let osJSON = JSON.parse(sectionElement.dataset['optionset']);
                        if (osJSON.name !== undefined)
                        {// HTML <section> has data-optionset, so treat is as an class Section optionSet member
                            let os : OptionSet | undefined = OptionSet.optionSetByName.get(osJSON.name);
                            if (os === undefined)
                            {
                                os = new OptionSet(osJSON.name);
                                OptionSet.optionSetByName.set(os.name,os);
                            }
                            console.assert(os !== null);
                            section.optionSet = os;
                            os.options.push(section);
                            section.optionIndex = os.options.length-1;
                            console.log("os.name: ",os.name,section.optionSet);
                        }
                    }
                }

                sectionElement.setAttribute("id",section.id);
    
                if (sectionElement.classList.contains("Instruction_Section"))
                {   
                    //if (level === 1 && section.optionSet !== null)
                    //  ISectionParent1 = new Instruction(section.sectionNumber + " (" + section.optionSet.name + (section.optionSet.options.length === 1 ? "" : ": Opt. " +  roman(section.optionIndex+1,Roman.UPPER)) + ")","",sectionName.trimStart().slice(0, 10) + " ...", Category.SECTION,'pointFraction' in sectionElement.dataset ? parseFloat(sectionElement.dataset.pointFraction) : 0,parent === null ? null : parent.instruction);
                    //else
                    ISectionParent1 = new Instruction(section.sectionNumber,"",sectionName.trimStart().slice(0, 10) + " ...", Category.SECTION,'pointFraction' in sectionElement.dataset ? parseFloat(sectionElement.dataset.pointFraction) : 0,parent === null ? null : parent.instruction);
                    this.instructions.push (ISectionParent1);
                    section.instruction = ISectionParent1;
                    console.log(sectionElement.dataset.pointFraction);
                    console.log(parent);
                    if (level === 1)
                        section.cumulativeFraction = parseFloat(sectionElement.dataset.pointFraction);                                            
                }
                this.collectInstructions(section,sectionElement, section.sectionNumber, ISectionParent1);
                this.extractSectionsAndRubric(section,sectionElement,section.level+1);
    
                if (h.className !== "nocount")
                    hc++;    
            }
        }
    }

    /**
     * @brief extractSectionsAndRubricAll traverses the DOM and all nested <section> elements and all nested <ol.Instruction> <li> elements, constructing 
     * a corresponding tree of Section objects and Instruction objects.
     **/
    extractSectionsAndRubricAll(totalPoints:number) 
    {        
        this.totalPoints = totalPoints;
        this.extractSectionsAndRubric(null,document.body,1);
        /**
         *  compute points from fraction hierarchy
         */
        for (let instruction of this.instructions) {
            let fraction : number = 1.0;
            for (let p = instruction; p != null; p = p.parent)
                fraction = fraction * (p.pointFraction / 100.0);
            instruction.points = this.totalPoints * fraction;
        }        

        console.log(this.instructions);
        console.log(Section.sections);
        this.displayRubric();        
    }
    //console.log("instructions.length:"+instructions.length);

    /**
     * @brief genreate the <tr> elemements in the Rubric <table> of the active HTML document
     */
    displayRubric() 
    {
        /*
         * construct <tbody> for <table> (#RubricTable) using instructions array and add
         * various <input> HTML elements to certain <table> columns
         */
        for (let osPair of OptionSet.optionSetByName)
        {
            const os = osPair[1]; 
            
            // append option suffix to instruction names.
            for (let o of os.options)
            {
                if (os.options.length === 1)
                    o.instruction.section += " - " + os.name + "";
                else
                    o.instruction.section += " - " + os.name + ": Opt. " +  roman(o.optionIndex+1,Roman.UPPER);
            }
        }

        let rubric: HTMLTableSectionElement = document.querySelector("#RubricTable > tbody");
        let prevSection: string = "";
        const REGEX = /Symbol\(([^)]*)\)/; // for removing Symbol sub-string
        let ri = 0;
        for (let instruction of this.instructions) {
            /**
             *  In the innerText of the Instruction <li> HTML::Element's, insert display of points allocated to this Instruction in the 
             */
            const iElement : HTMLElement = document.getElementById(instruction.id);
            if (iElement !== null)
            {
                const ptDiv = <HTMLDivElement> document.createElement("span");
                ptDiv.classList.add("Points");
                let points : string = instruction.points.toFixed(1);
                if (points.split('.')[1]==='0') 
                    points = points.split('.')[0];
                else if (points.split('.')[0]==='0') 
                    points = "." + points.split('.')[1];                

                if (instruction.category === Category.COMPOSITE)
                    ptDiv.innerHTML="[" + points + " pt]"; //ptDiv.innerHTML="[ &#x2211;" + points + " pt]";                        
                else
                    ptDiv.innerHTML="[" + points + " pt]";                        
                if (iElement.firstElementChild instanceof HTMLSpanElement && iElement.firstElementChild.classList.contains("Instruction_Title"))
                {
                    ptDiv.innerHTML = "&nbsp;" + ptDiv.innerHTML;
                    iElement.firstElementChild.after(ptDiv);
                }
                else
                    //iElement.insertAdjacentHTML("afterbegin","|<sup>"+ptDiv.innerHTML + "</sup>| &nbsp;");
                    iElement.insertAdjacentHTML("afterbegin","|<sup>"+ptDiv.innerHTML + "</sup>| &nbsp;");
            }
            
            /**
             *   In the Rubric table, insert rows corresponding to all Instructions
             */              
            let row = document.createElement("tr");
            row.setAttribute("data-ri", ri.toString());    // 'ri' abbr. 'rowIndex'
            let levelPrefix="";  // string used to display ASCII art tree diagram
            let level=0;  // 'level' is the depth in the Instruction tree of 'instruction'

            // construct ASCII art tree diagram
            for (let i=instruction; i != null; i = i.parent)
                {
                    if (level>0)
                    {
                        if (i.parent)
                        {
                            let pi : number = i.parent.subSteps.findIndex((e)=> e==i);  // 'parentIndex'
                            if (pi < i.parent.subSteps.length-1)
                                // current instruction parent is a child of the grandparent with further children, so at |
                                levelPrefix = "|&nbsp;&nbsp;&nbsp;&nbsp;" + levelPrefix; 
                            else
                                levelPrefix = "&nbsp;&nbsp;&nbsp;&nbsp;" + levelPrefix; 
                        }
                        else
                            levelPrefix = "&nbsp;&nbsp;&nbsp;&nbsp;" + levelPrefix; 
                    }
                    level++; 
                }
            if (instruction.parent !== null)                
                levelPrefix += "|---";

            // add level info to <td> HTML elment -- might be useful in future                
            row.setAttribute("data-level", level.toString());
            
            // pad pointFraction string representation to length of 4 using spaces
            let pf : string = instruction.pointFraction.toFixed(0);  // abbr. 'pointFraction'
            let len = 4-pf.length;
            for (let i = 0;i<len;i++)
                pf = "&nbsp" + pf;

            // pad point string representation to length of 4 using spaces
            let ps : string = instruction.points.toFixed(2);  // abbr. 'pointString'
            len = 4-ps.length;
            for (let i = 0;i<len;i++)
                ps = "&nbsp" + ps;                

            // disable checkbox for instructions worth 0 points.                
            let disabled = "";
            if (instruction.points === 0.0)
                disabled = "disabled";

            // created row's HTML code                 
            if (instruction.section === prevSection)            
                row.innerHTML = 
                `<td class="Empty"></td>`;
            else
            {
                //if (instruction.gui_checkbox)
                row.innerHTML =
                `   <td>${instruction.section}</td>`;
            }
                row.innerHTML +=                 
                 `<td>${instruction.number}</td>
				 <td>${Category[instruction.category].toLowerCase()}</td>
				 <td><a href="#${instruction.id}" onclick="BreadCrumb.onclick(this);">${instruction.short}</a></td>                 
                 <td style='width:fit-content;'><span>${levelPrefix}</span>${pf}&percnt;</td>
                 <td>
                    <span>${levelPrefix}</span>${ps}
                     <span hidden class="Instructor_Mode" style="padding: 4px; border: solid 1px black">
                        <span><input type="checkbox" id="#CB_${instruction.id}" name="scales" ${disabled}></span>
                        <span style="margin-left:10px"> 0.00 
                    </span>                        
                </td>                                                                        
                 <td class="Instructor_Mode" hidden> <form><textarea rows='1'></textarea></form> </td>`;

            //<td class="Instructor_Mode" hidden> <input type="text"> </td>`;
            //<td class="Instructor_Mode" hidden> <form><textarea rows='1'></textarea></form> </td>
            //<td><span style="color:white; border : ${level!=0?'solid':'none'} 1px black;">${levelPrefix}</span>${instruction.pointFraction.toFixed(0)}&percnt;</td>
            //<td><div style="vertical-align: 2px; display : inline-block; color:white; border : ${level!=0?'solid':'none'} 1px black; height: 5px; width : ${level*25}px"></div>${instruction.pointFraction.toFixed(0)}&percnt;</td>
            /*
            <td><div style="vertical-align: 2px; display : inline-block; color:white; border : ${level!=0?'solid':'none'} 1px black; height: 5px; width : ${level*25}px"></div>${instruction.points.toFixed(2)}
                    <span hidden class="Instructor_Mode" style="padding: 4px; border: solid 1px black">
                        <span><input type="checkbox" id="#CB_${instruction.id}" name="scales" ${disabled}></span>
                        <span style="margin-left:10px"> 0.00 </span>
                    </span>
            </td>                                  
            */
            const input = <HTMLInputElement>row.querySelector("input[type='checkbox']");
            input.addEventListener('change',(e : InputEvent)=>{instruction.gui_checkbox(<HTMLInputElement>e.target);});

            prevSection = instruction.section;                        
            row.querySelector('textarea').addEventListener('change',
                (e: Event) => {
                    const ta = (<HTMLTextAreaElement>e.target);
                    console.log( ta.value) ;
                    ta.innerText = ta.value;
                    /*
                    const itemID = (<HTMLInputElement>(e.target)).parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.querySelector('a').getAttribute('href');
                    const rowIndex = parseInt((<HTMLInputElement>(e.target)).parentElement.parentElement.getAttribute("data-ri"));
                    console.log(itemID.slice(1) + ":" + rowIndex + ":" + e);
                    console.log(instructions.instructions[rowIndex]);
                    instructions.instructions[rowIndex].comment = (<HTMLInputElement>(e.target)).value;
                    */
                });

            /*
            row.querySelector('input[type="text"]').addEventListener('input',
                (e: Event) => {
                    const itemID = (<HTMLInputElement>(e.target)).parentElement.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.querySelector('a').getAttribute('href');
                    const rowIndex = parseInt((<HTMLInputElement>(e.target)).parentElement.parentElement.getAttribute("data-ri"));
                    console.log(itemID.slice(1) + ":" + rowIndex + ":" + e);
                    console.log(instructions.instructions[rowIndex]);
                    instructions.instructions[rowIndex].comment = (<HTMLInputElement>(e.target)).value;
                });
            */
            rubric.append(row);
            ri++;
        }
        //let ttd = document.getElementById("Total");
        const tps = document.querySelectorAll("span[data-total-points]")
        for ( let tp of tps)
            (<HTMLElement>tp).innerText = this.totalPoints.toString();

        /************************************************************************************
         *  In the Rubric Awarded Points Table, add the generated Sequences
         *  [wip] 9/15/2023
         ************************************************************************************/
        const rapTable = <HTMLTableElement>document.getElementById("RubricAwardedPoints");
        const sequences0 : Array<Sequence> = new Array<Sequence>(),
              sequences1 : Array<Sequence> = new Array<Sequence>();
        const sequences = [sequences0,sequences1];
        let last=0,next=1;

        sequences[last] 
        for (let osPair of OptionSet.optionSetByName)
        {
            const os = osPair[1];
            console.log(os);   
            
            // construct and update the sequences
            sequences[next].length=0;             
            if (sequences[last].length === 0)
            {                                    
                for (let so of os.options)
                {              
                    const se = new Sequence();                    
                    se.sections.push(so);
                    try{
                        //console.assert(so.instruction !== undefined);
                        se.totalPoints += so.instruction.points;
                    }
                    catch (e)
                    {
                        throw e; 
                    }
                    
                    se.totalPoints += so.instruction.points;
                    sequences[next].push(se);         
                }
                console.log(sequences[next]);     
            }
            else
                for (let i=0;i< sequences[last].length;i++)
                {                                    
                    for (let so of os.options)
                    {
                        const se = new Sequence();
                        se.sections = Array.from(sequences[last][i].sections);
                        se.sections.push(so);
                        sequences[next].push(se);         
                    }
                    console.log(sequences[next]);     
                }
            [last,next] = [next,last];
        }
        let sequenceFraction = 1.0;
        for (let s of sequences[last])
        {
            const tbody=<HTMLTableSectionElement> rapTable.querySelector(":scope tbody");
            const tr = document.createElement('tr');
            tbody.appendChild(tr);
            let innerHTML : string = "";
            innerHTML="<td>";
            let si = 0;
            let sequenceFraction = 0;
            for (let se of s.sections)
            {
                innerHTML += `${se.optionSet?.name}` + (se.optionSet.options.length === 1 ? "" : `-${roman(se.optionIndex+1,Roman.UPPER)}`) + ", ";
                sequenceFraction += se.cumulativeFraction;
                si++;
            }
            innerHTML +="</td>";
            //innerHTML +=`<td><input type="checkbox" disabled></input></td>`;
            innerHTML +=`<td>${(sequenceFraction/100*this.totalPoints).toFixed(2)}</td><td></td>`;
            tr.innerHTML = innerHTML;            
        }

    }
}
export const instructions = new Instructions();

/**
 **
 **  INTERNAL FUNCTIONS
 **
 */

 /**
  * @brief convert list (an array) of item numbers for a set of nested items into a string using the convention of
  *                    [Decimal Number] . [Lower Case Letter] . [Lower Case Roman Numeral]
  * @param args 
  * @returns string
  */
function itemString(...args: number[]) {
    const aCode = "a".charCodeAt(0);
    switch (arguments.length) {
        case 1:
            return args[0].toString();
        case 2:
            return args[0].toString() + "." + String.fromCharCode(aCode + args[1] - 1);
        case 3:
            return args[0].toString() + "." + String.fromCharCode(aCode + args[1] - 1) + "." + roman(args[2]);
        case 4:
            return args[0].toString() + "." + String.fromCharCode(aCode + args[1] - 1) + "." + roman(args[2]) + "." + args[3].toString();           
    }
}

const ROMAN_VALUE = Uint16Array.from(
    [
        1000,
        900,
        500,
        400,
        100,
        90,
        50,
        40,
        10,
        9,
        5,
        4,
        1
    ]);

const ROMAN_SYMBOL : Array<string> = new Array<string>(
    
        "m",
        "cm",
        "d",
        "cd",
        "c",
        "xc",
        "l",
        "xl",
        "x",
        "ix",
        "v",
        "iv",
        "i"
    );

enum Roman
{
    UPPER,
    LOWER
};

function roman(n : number,r : Roman = Roman.LOWER): string 
{
    let str = "";
    for (let i = 0; i < 13; i++) {
        const v = ROMAN_VALUE[i];
        let q = Math.floor(n / v);
        n -= q * v;
        const rs : string = r === Roman.LOWER ? ROMAN_SYMBOL[i] : ROMAN_SYMBOL[i].toUpperCase();
        str += rs.repeat(q);
    }
    return str;
}
