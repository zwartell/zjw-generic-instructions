## ZJW Generic Instructions
### Author: Prof. Zachary Wartell, UNC Charlotte <br> https://webpages.charlotte.edu/zwartell/

HTML5 (HTML, CSS, Typescript) library module used by Dr. Wartell's courses' assignment instructions.

Links to some example assignment instructions can be found at https://webpages.charlotte.edu/zwartell/.
